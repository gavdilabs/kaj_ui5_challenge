sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/Fragment"
], function (Controller, Fragment) {
	"use strict";

	return Controller.extend("sap.ui.challenge.controller.Detail", {
		onInit: function () {
			this.oOwnerComponent = this.getOwnerComponent();

			this.oRouter = this.oOwnerComponent.getRouter();
			this.oModel = this.oOwnerComponent.getModel();

			this.oRouter.getRoute("master").attachPatternMatched(this._onProductMatched, this);
			this.oRouter.getRoute("detail").attachPatternMatched(this._onProductMatched, this);
			this.oRouter.getRoute("detailDetail").attachPatternMatched(this._onProductMatched, this);
		},

		onPurchasePress: function (oEvent) {
			var supplierPath = oEvent.getSource().getBindingContext("products").getPath(),
				supplier = supplierPath.split("/").slice(-1).pop(),
				oNextUIState;

			this.oOwnerComponent.getHelper().then(function (oHelper) {
				oNextUIState = oHelper.getNextUIState(2);
				this.oRouter.navTo("detailDetail", {
					layout: oNextUIState.layout,
					supplier: supplier,
					product: this._product
				});
			}.bind(this));
		},

		_onProductMatched: function (oEvent) {
			this._product = oEvent.getParameter("arguments").product || this._product || "0";
			this.getView().bindElement({
				path: "/Products/" + this._product,
				model: "products"
			});

			var productID = this.getView().getModel("products").getProperty("/Products/" + this._product + "/productID");
			/*
			alert("product id" + productID);
			*/
			var myTable = this.getView().byId("purchasesId");

			var oTableSearchState = [new sap.ui.model.Filter("product", sap.ui.model.FilterOperator.EQ, productID)];
			myTable.getBinding("items").filter(oTableSearchState, "Application");


		},

		onEditToggleButtonPress: function () {
			var oObjectPage = this.getView().byId("ObjectPageLayout"),
				bCurrentShowFooterState = oObjectPage.getShowFooter();

			oObjectPage.setShowFooter(!bCurrentShowFooterState);
		},

		handleFullScreen: function () {
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/fullScreen");
			this.oRouter.navTo("detail", { layout: sNextLayout, product: this._product });
		},

		handleExitFullScreen: function () {
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/exitFullScreen");
			this.oRouter.navTo("detail", { layout: sNextLayout, product: this._product });
		},

		handleClose: function () {
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/closeColumn");
			this.oRouter.navTo("master", { layout: sNextLayout });
		},

		onExit: function () {
			this.oRouter.getRoute("master").detachPatternMatched(this._onProductMatched, this);
			this.oRouter.getRoute("detail").detachPatternMatched(this._onProductMatched, this);
		},

		onCompanyInfoPress: function () {
			var oView = this.getView();
			

			//create dialog fragmnet
			if (!this.byId("companyInfo")) {
				//load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "sap.ui.challenge.view.CompanyInfo",
					controller: this
				}).then(function (oDialog) {
					//connect dialog to rootview of this component
					oView.addDependent(oDialog);
					oDialog.open();
				})
			} else {
				this.byId("companyInfo").open();
			}
		},

		onCloseCompanyInfo: function () {
			this.byId("companyInfo").close();
		},

		onNewPurchasePress: function () {
			var oModel = this.getView().getModel("purchases");
			var PurchasesList = oModel.getProperty("/Purchases").length;
			var NewPurchaseID = PurchasesList + 1;

			var dialog = new sap.m.Dialog({
				title: "New Purchase",
				type: "Message",
				content: [
					new sap.ui.layout.HorizontalLayout({

						content: [new sap.ui.layout.VerticalLayout({

							width: "150px",

							content: [
							new sap.m.Label({ text: "Purchase ID" }),
							new sap.m.Input("purchaseID", { value: NewPurchaseID, editable: false }),
							new sap.m.Label({ text: "Customer Name" }),
							new sap.m.Input("CustomerName"),
							new sap.m.Label({ text: "Amount" }),
							new sap.m.Input("Amount")
							]

						})]
					})],

				beginButton: new sap.m.Button({

					text: "Save",

					press: function () {
						var NewPurchaseID = sap.ui.getCore().byId("purchaseID").getValue();
						var CustomerName = sap.ui.getCore().byId("CustomerName").getValue();
						var Amount = sap.ui.getCore().byId("Amount").getValue();

						var oPurchase = {};
						oPurchase = {
							"purchaseID": NewPurchaseID,
							"createdBy": CustomerName,
							"amount": Amount
						};

						var oPurchases = oModel.getProperty("/Purchases");
						oPurchases.push(oPurchase);
						oModel.setProperty("/Purchases", oPurchases);

						dialog.close();
					}
				}),

				endButton: new sap.m.Button({
					text: "Cancel", press: function () {
						dialog.close();
					}
				}),

				afterClose: function () {

					dialog.destroy();

				}
			});

			dialog.open();
		},

	});
});

